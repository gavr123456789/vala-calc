/* window.vala
 *
 * Copyright 2018 gavr
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
//using Lua;

namespace Buildertest2 {
	[GtkTemplate (ui = "/org/gnome/Buildertest2/window.ui")]
	public class Window : Gtk.ApplicationWindow {
		[GtkChild]
		Gtk.Button b1;
		[GtkChild]
		Gtk.Button b2;
		[GtkChild]
		Gtk.Button b3;
		[GtkChild]
		Gtk.Button b4;
		[GtkChild]
		Gtk.Button b5;
		[GtkChild]
		Gtk.Button b6;
		[GtkChild]
		Gtk.Button b7;
		[GtkChild]
		Gtk.Button b8;
		[GtkChild]
		Gtk.Button b9;
		[GtkChild]
		Gtk.Button b0;
		[GtkChild]
		Gtk.Button bplus;
		[GtkChild]
		Gtk.Button bmin;
		[GtkChild]
		Gtk.Button bumn;
		[GtkChild]
		Gtk.Button bdel;
		[GtkChild]
		Gtk.Button bravno;
		[GtkChild]
		Gtk.Entry entryField;
		
		string str1=""; 
		string str2=""; 
		string str="";
	    
	    string strAction="";
	    int a=0; int b=0; 
	    int result=0;
	    
	    bool action=false;
		
		void sas(string rar){
		    if(action==false)str1+=rar;
		    else str2+=rar;str=str1+strAction+str2; entryField.set_text(str);
		}
		void actionMeth(string rer){
		    if (!action){
		    strAction=rer;str+=rer;}
		        action=true;
		        entryField.set_text(str);
		}
		

	
		public Window (Gtk.Application app) {
			Object (application: app);
			
		    b0.clicked.connect(()=>  sas("0"));
		    b1.clicked.connect(()=>  sas("1"));
		    b2.clicked.connect(()=>  sas("2"));
		    b3.clicked.connect(()=>  sas("3"));
		    b4.clicked.connect(()=>  sas("4"));
		    b5.clicked.connect(()=>  sas("5"));
		    b6.clicked.connect(()=>  sas("6"));
		    b7.clicked.connect(()=>  sas("7"));
		    b8.clicked.connect(()=>  sas("8"));
		    b9.clicked.connect(()=>  sas("9"));
		    
		    bdel.clicked.connect(()=>actionMeth("/"));
		    bmin.clicked.connect(()=>actionMeth("-"));
		    bplus.clicked.connect(()=>actionMeth("+"));
		    bumn.clicked.connect(()=>actionMeth("*"));
		    
		    bravno.clicked.connect(()=>{
		        a=int.parse(str1);
		        b=int.parse(str2);
			        
		        if (strAction=="+")result=a+b;
		        if (strAction=="-")result=a-b;
		        if (strAction=="*")result=a*b;
		        if (strAction=="/")result=a/b;
		        
		        
		        entryField.set_text(result.to_string());
		        action=false;
		        str1="";str2="";str="";
		        
		    });
		}	
	}
}
