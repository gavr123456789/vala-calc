#ifndef __RESOURCE_buildertest2_H__
#define __RESOURCE_buildertest2_H__

#include <gio/gio.h>

G_GNUC_INTERNAL GResource *buildertest2_get_resource (void);
#endif
